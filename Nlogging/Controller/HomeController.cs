﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Nlogging.Controller
{
    public class HomeController:Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            _logger.LogCritical(1, "NLog injected into HomeController");
        }
        public IActionResult Index()
        {
            throw new Exception("test2");
            ViewBag.Name = "Sandeep";
            _logger.LogError("Test");
            return View();
        }
    }
}
